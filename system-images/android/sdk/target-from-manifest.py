import sys
from xml.etree import ElementTree as ET

for arg in sys.argv[1:]:
    tree = ET.parse(arg)
    targets = {}

    prefix = '{http://schemas.android.com/apk/res/android}'
    for md in tree.findall("application/activity/meta-data"):
        if md.attrib[prefix + 'name'] == 'android.app.lib_name':
            targetName = md.attrib[prefix + 'value']
            if not targetName in targets:
                print(targetName)
                targets[targetName] = 1
