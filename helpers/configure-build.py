#!/usr/bin/python3
import os
import sys
import argparse
import subprocess
from helperslib import BuildSpecs, BuildSystem, CommonUtils, EnvironmentHandler

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to configure a project to be built.')
parser.add_argument('--product', type=str, required=True)
parser.add_argument('--project', type=str, required=True)
parser.add_argument('--branchGroup', type=str, required=True)
parser.add_argument('--platform', type=str, required=True)
parser.add_argument('--installTo', type=str, required=True)
arguments = parser.parse_args()

# Load our build specification, which governs how we handle this build
buildSpecification = BuildSpecs.Loader( product=arguments.product, project=arguments.project, branchGroup=arguments.branchGroup, platform=arguments.platform )

# Determine the environment we need to provide to any configure system (like CMake or Autotools)
buildEnvironment = EnvironmentHandler.generateFor( installPrefix=arguments.installTo )

# Environment variables which shouldn't be exposed publicly (as they expose internal innards of our CI system or other secrets)
variablesToIgnore = ['HUDSON_COOKIE', 'HUDSON_SERVER_COOKIE', 'JENKINS_SERVER_COOKIE', 'JENKINS_NODE_COOKIE', 'JENKINS_HOME', 'SSH_CLIENT', 'SSH_CONNECTION']

# To assist developers with debugging we'll now print out the build environment we're going to use
print("== Using the following environment to perform the build:")
for variableName, variableEntries in buildEnvironment.items():
    # Are we allowed to print this out?
    if variableName in variablesToIgnore:
        # Then we skip to the next one
        continue

    # Determine how we want to present this particular variable
    prettyMessage = "{0:25} = '{1}'".format(variableName, variableEntries)
    print(prettyMessage)

# Seperate the actual configure command output clearly from the environment variables
print("\n\n== Commencing Configuration:")

# Determine where our source code is checked out to and where we will be building it
# We'll assume that the directory we're running from is where the sources are located
sourcesLocation = os.getcwd()
buildLocation = CommonUtils.buildDirectoryForSources( sources=sourcesLocation, inSourceBuild=buildSpecification['in-source-build'] )

# Make sure the build location exists
if not os.path.isdir(buildLocation):
	os.makedirs(buildLocation)

# Detect the appropriate build system to use
# We always do this even if the build specification says not to in order to keep the code cleaner
buildSystemHandler = BuildSystem.detect( sourcesLocation )

# Are we allowed to configure the build system using the handler?
if buildSpecification['detect-build-system'] and buildSystemHandler is not None:
	# Ask the build system handler to configure the project
	if not buildSystemHandler.configure( buildLocation, sourcesLocation, arguments.installTo, buildSpecification, buildEnvironment ):
		# Looks like the handler failed to configure the project - let's bail!
		sys.exit(1)

# Now we run any additional configure commands specified in the build specification
for configureCommand in buildSpecification['configure-commands']:
	# Now run it
	try:
		commandToRun = BuildSystem.substituteCommandTokens( configureCommand, sources=sourcesLocation, installPrefix=arguments.installTo )
		subprocess.check_call( commandToRun, stdout=sys.stdout, stderr=sys.stderr, shell=True, cwd=buildLocation, env=buildEnvironment )
	except Exception:
		sys.exit(1)

# All done!
sys.exit(0)
